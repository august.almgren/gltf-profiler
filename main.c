#include "main.h"

static void print_fps()
{
	// called once per frame
	// print fps to stdout once per time interval
	float curr_time = glfwGetTime();
	const double PRINT_FPS_INTERVAL_SECONDS = 1.0;
	static time_t last_time = 0;
	static unsigned int fps_counter = 0;
	if (curr_time - last_time  >= PRINT_FPS_INTERVAL_SECONDS) {
		printf("fps: %d\n", (unsigned int)(fps_counter / PRINT_FPS_INTERVAL_SECONDS));
		last_time = curr_time;
		fps_counter = 0;
	} else {
		fps_counter++;
	}

}

static void glfw_error_callback(int error, const char *description)
{
	printf("GLFW error %d: %s\n", error, description);
}

//#define MODEL_DIRNAME "data/box/"
//#define MODEL_FILENAME "Box.gltf"
//#define MODEL_DIRNAME "data/sponza/"
//#define MODEL_FILENAME "Sponza.gltf"
#define MODEL_DIRNAME "data/suzanne/"
#define MODEL_FILENAME "Suzanne.gltf"

int main() {

	// setup OpenGL
	glewExperimental = true;
	if (!glfwInit()) {
		printf("couldn't init glfw\n");
		return 0;
	}
	glfwSetErrorCallback(glfw_error_callback);

	GLFWwindow *window = init_window();
	if (!window) {
		printf("couldn't init window\n");
		glfwTerminate();
		return 0;
	}

	if (glewInit() != GLEW_OK) {
		printf("couldn't init glew\n");
		glfwTerminate();
		return 0;
	}

	struct shader_t *shader = load_shader("vert.glsl", "frag.glsl");
	if (!shader) {
		printf("couldn't load shader\n");
		return 0;
	}
	glUseProgram(shader->id);

	clear_window();
	glfwSwapBuffers(window);

	struct scene_t *scene = load_active_scene(MODEL_DIRNAME, MODEL_FILENAME);
	if (!scene) {
		printf("couldn't load active gltf scene\n");
		return 0;
	}

	glEnable(GL_DEPTH_TEST);
	setup_scene_rendering(scene);

	init_settings();

	printf("enter render loop\n");
	while (!glfwWindowShouldClose(window)) {
		glUniform1f(shader->uniforms.curr_time, glfwGetTime());
		clear_window();
		render_scene(scene, shader);
		glfwSwapBuffers(window);
		glfwPollEvents();
		print_fps();
	}
	printf("exit render loop\n");

	glfwDestroyWindow(window);
	glDeleteProgram(shader->id);
	glfwTerminate();
	//free_shader(shader_id);
	//free_scene(scene);

	return 0;
}
