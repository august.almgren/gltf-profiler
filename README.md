# glTF 2.0 Profiler

This profiler loads and renders glTF 2.0 files. As a model is rendered, the number of rendered frames per second (fps) is sampled once a second and output to stdout.

The `OpenGL`, `GLFW` and `GLEW` libraries are required to execute the profiler. `clang` is also required to build.

The profiler was developed and tested on Ubuntu 16.04.

The file directory and filename of the model to be rendered should be set in main.c before compilation.

Compilation and linking is handled in the Makefile. There are two ways to build: `make debug` and `make release`. `make clean` will remove the generated files. The executable is titled `gltf-profiler-debug` or `gltf-profiler-release` depending on the build type.

Each model requires a separate model-view matrix to be visible. Currently this needs to be defined per model in `vert.glsl`. Working matrices have been provided for all models currently in `data/`, and the one being rendered should be the only one that is uncommented. Modifying `vert.glsl` can be done without a rebuild as the shaders are loaded at runtime.

The controls (see `window.c` and `settings.c`) are as follows:

* `ESC` - quit
* `A` - toggle alpha to coverage
* `B` - toggle blending
* `C` - toggle scissor test
* `D` - toggle depth test
* `I` - toggle dithering
* `M` - toggle max fps
* `N` - toggle anti-aliasing
* `P` - toggle polygon offset fill
* `S` - toggle sample coverage
* `T` - toggle stencil test
* `U` - toggle backface culling
* `W` - toggle wireframe mode

When a flag is toggled, the event is output to stdout.

The glTF 2.0 reference specification can be found [here](https://github.com/KhronosGroup/glTF/tree/master/specification/2.0).

The sample models used have been sourced from [here](https://github.com/KhronosGroup/glTF-Sample-Models/tree/master/2.0).

There is currently a bug which occasionally causes a crash when compiling `frag.glsl`.
