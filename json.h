#ifndef JSON_H
#define JSON_H

#include <stdbool.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "lib/json-parser/json.h"

json_value *load_json(const char *fname, const char *fpath);
void free_json(json_value *json);

unsigned int json_int(const json_value *v);
double json_dbl(const json_value *v);
bool json_bool(const json_value *v);
unsigned int *json_intarr(const json_value *v);
double *json_dblarr(const json_value *v);
bool *json_boolarr(const json_value *v);
char *json_str(const json_value *v);
unsigned int json_strlen(const json_value *v);

unsigned int json_arrlen(const json_value *v);
json_value *json_arrval(const json_value *v, const unsigned int i);
unsigned int json_arrint(const json_value *v, const unsigned int i);
double json_arrdbl(const json_value *v, const unsigned int i);
bool json_arrbool(const json_value *v, const unsigned int i);
unsigned int *json_arrintarr(const json_value *v, const unsigned int i);
double *json_arrdblarr(const json_value *v, const unsigned int i);
bool *json_arrboolarr(const json_value *v, const unsigned int i);
char *json_arrstr(const json_value *v, const unsigned int i);
unsigned int json_arrstrlen(const json_value *v, const unsigned int i);

unsigned int json_objlen(const json_value *v);
char *json_objname(const json_value *v, const unsigned int i);
bool json_isobjname(const json_value *v, const unsigned int i, const char *s);
json_value *json_objval(const json_value *v, const unsigned int i);
unsigned int json_objint(const json_value *v, const unsigned int i);
double json_objdbl(const json_value *v, const unsigned int i);
bool json_objbool(const json_value *v, const unsigned int i);
unsigned int *json_objintarr(const json_value *v, const unsigned int i);
double *json_objdblarr(const json_value *v, const unsigned int i);
bool *json_objboolarr(const json_value *v, const unsigned int i);
char *json_objstr(const json_value *v, const unsigned int i);
unsigned int json_objstrlen(const json_value *v, const unsigned int i);

#endif // JSON_H
