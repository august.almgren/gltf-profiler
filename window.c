#include "window.h"

static void glfw_key_callback(GLFWwindow *window, int key, int scancode, int action, int mods)
{
	if (action == GLFW_PRESS) {
		switch(key) {
		case GLFW_KEY_ESCAPE:
			glfwSetWindowShouldClose(window, true);
			break;
		case GLFW_KEY_A:
			toggle_sample_alpha_to_coverage();
			break;
		case GLFW_KEY_B:
			toggle_blending();
			break;
		case GLFW_KEY_C:
			toggle_scissor_test();
			break;
		case GLFW_KEY_D:
			toggle_depth_test();
			break;
		case GLFW_KEY_I:
			toggle_dithering();
			break;
		case GLFW_KEY_M:
			toggle_max_fps();
			break;
		case GLFW_KEY_N:
			toggle_anti_aliasing();
			break;
		case GLFW_KEY_P:
			toggle_polygon_offset_fill();
			break;
		case GLFW_KEY_S:
			toggle_sample_coverage();
			break;
		case GLFW_KEY_T:
			toggle_stencil_test();
			break;
		case GLFW_KEY_U:
			toggle_backface_culling();
			break;
		case GLFW_KEY_W:
			toggle_wireframe_mode();
			break;
		}
	}
}

static void glfw_resize_callback(GLFWwindow *window, int width, int height)
{
	int viewport_width, viewport_height;
	glfwGetFramebufferSize(window, &viewport_width, &viewport_height);
	glViewport(0, 0, viewport_width, viewport_height);
}

GLFWwindow *init_window()
{
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	const GLFWvidmode *vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());
	const int w = vidmode->width / 2;
	const int h = vidmode->height / 2;
	printf("window size: %d x %d\n", w, h);

	GLFWwindow *window = glfwCreateWindow(w, h, "GLTF Profiler", NULL, NULL);
	if (!window) {
		printf("couldn't init window\n");
		return NULL;
	}

	glfwMakeContextCurrent(window);

	glfwSetWindowPos(window, vidmode->width / 4, vidmode->height / 4);
	glfwSetKeyCallback(window, glfw_key_callback);
	glfwSetWindowSizeCallback(window, glfw_resize_callback);

	glClearColor(0.0, 0.0, 0.0, 1.0);
	clear_window();
	glfwSwapBuffers(window);

	return window;
}

void clear_window()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}
