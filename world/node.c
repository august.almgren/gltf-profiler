#include "world/node.h"

struct node_t *load_node(const struct gltf_node_t *gltf_node)
{
	struct node_t *node;
	node = malloc(sizeof(*node));

	node->num_children = gltf_node->num_children;

	for (unsigned int i = 0; i < 16; i++) {
		node->matrix[i] = gltf_node->matrix[i];
	}

	node->num_refs = 1;
	return node;
}

void link_node(struct node_t *node, const struct gltf_node_t *gltf_node,
		struct world_lists_t *lists)
{
	if (gltf_node->mesh.exists) {
		node->mesh = lists->meshes[gltf_node->mesh.accessor_id];
		node->mesh->num_refs++;
	} else {
		node->mesh = NULL;
	}

	if (node->num_children > 0) {
		node->children = malloc(node->num_children * sizeof(*node->children));
		for (unsigned int i = 0; i < node->num_children; i++) {
			node->children[i] = lists->nodes[gltf_node->child_ids[i]];
			node->children[i]->num_refs++;
		}
	} else {
		node->children = NULL;
	}
}

void setup_node_rendering(struct node_t *node)
{
	if (node->mesh) {
		setup_mesh_rendering(node->mesh);
	}

	for (unsigned int i = 0; i < node->num_children; i++) {
		setup_node_rendering(node->children[i]);
	}
}

void render_node(const struct node_t *node, const struct shader_t *shader)
{
	//glUniformMatrix4fv(shader->uniforms.model_matrix, 1, GL_FALSE, node->matrix);

	if (node->mesh) {
		render_mesh(node->mesh);
	}

	for (unsigned int i = 0; i < node->num_children; i++) {
		render_node(node->children[i], shader);
	}
}

void deref_node(struct node_t *node)
{
	if (--node->num_refs == 0) {

		if (node->mesh) {
			deref_mesh(node->mesh);
		}

		if (node->children) {
			for (unsigned int i = 0; i < node->num_children; i++) {
				deref_node(node->children[i]);
			}
			free(node->children);
		}

		free(node);
	}
}
