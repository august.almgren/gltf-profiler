#include "world/buffer.h"

struct buffer_t *load_buffer(const struct gltf_buffer_t *gltf_buffer)
{
	struct buffer_t *buffer;
	buffer = malloc(sizeof(*buffer));

	buffer->data = malloc(gltf_buffer->len);
	FILE *f = fopen(gltf_buffer->fpath, "r");
	fread(buffer->data, 1, gltf_buffer->len, f);
	fclose(f);

	buffer->num_refs = 1;
	return buffer;
}

void deref_buffer(struct buffer_t *buffer)
{
	if (--buffer->num_refs == 0) {
		free(buffer->data);
		free(buffer);
	}
}
