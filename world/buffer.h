#ifndef WORLD_BUFFER_H
#define WORLD_BUFFER_H

#include <stdio.h>
#include <stdlib.h>

#include "gltf/buffer.h"

struct buffer_t {
	void *data;
	unsigned int num_refs;
};

struct buffer_t *load_buffer(const struct gltf_buffer_t *gltf_buffer);
void deref_buffer(struct buffer_t *buffer);

#endif // WORLD_BUFFER_H
