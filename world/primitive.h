#ifndef WORLD_PRIMITIVE_H
#define WORLD_PRIMITIVE_H

#include <GL/glew.h>
#include <stdio.h>
#include <stdlib.h>

#include "gltf/primitive.h"
#include "world/accessor.h"
#include "world/lists.h"

struct primitive_t {
	struct accessor_t *positions;
	struct accessor_t *indices;
	unsigned int mode;
	unsigned int vao;
};

struct primitive_t *load_primitive(const struct gltf_primitive_t *gltf_primitive);
void link_primitive(struct primitive_t *primitive, const struct gltf_primitive_t *gltf_primitive,
		struct world_lists_t *lists);
void setup_primitive_vao(struct primitive_t *primitive);
void render_primitive(const struct primitive_t *primitive);
void deref_primitive(struct primitive_t *primitive);

#endif // WORLD_PRIMITIVE_H
