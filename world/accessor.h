#ifndef WORLD_ACCESSOR_H
#define WORLD_ACCESSOR_H

#include <GL/glew.h>
#include <stdio.h>
#include <stdlib.h>

#include "gltf/accessor.h"
#include "world/bufferview.h"
#include "world/lists.h"

struct accessor_t {
	struct bufferview_t *bufferview;
	unsigned int offset; // from bufferview start
	unsigned int component_type; // float, unsigned int, ...
	unsigned int component_size; // 1, 2, 3, 4
	unsigned int count;
	unsigned int num_refs;
};

struct accessor_t *load_accessor(const struct gltf_accessor_t *gltf_accessor);
void link_accessor(struct accessor_t *accessor,
		const struct gltf_accessor_t *gltf_accessor, struct world_lists_t *lists);
void setup_accessor_vbo(const GLuint attr_index, struct accessor_t *accessor);
void setup_accessor_ebo(struct accessor_t *accessor);
void deref_accessor(struct accessor_t *accessor);

#endif // WORLD_ACCESSOR_H
