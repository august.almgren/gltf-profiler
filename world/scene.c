#include "world/scene.h"

struct scene_t *load_active_scene(const char *dirname, const char *filename)
{
	printf("loading %s%s\n", dirname, filename);

	struct gltf_t *gltf = load_gltf(dirname, filename);
	if (!gltf) {
		return NULL;
	}
	printf("gltf load complete\n");

	struct world_lists_t *lists = load_world_lists(gltf);
	free_gltf(gltf);

	struct scene_t *active_scene = lists->scene;
	active_scene->num_refs++;

	deref_world_lists(lists);

	return active_scene;
}

struct scene_t *load_scene(const struct gltf_scene_t *gltf_scene)
{
	struct scene_t *scene;
	scene = malloc(sizeof(scene));

	scene->num_nodes = gltf_scene->num_nodes;
	scene->nodes = malloc(sizeof(*scene->nodes) * gltf_scene->num_nodes);

	scene->num_refs = 1;
	return scene;
}

void link_scene(struct scene_t *scene, const struct gltf_scene_t *gltf_scene,
		struct world_lists_t *lists)
{
	for (unsigned int i = 0; i < gltf_scene->num_nodes; i++) {
		scene->nodes[i] = lists->nodes[gltf_scene->node_ids[i]];
		scene->nodes[i]->num_refs++;
	}
}

void setup_scene_rendering(struct scene_t *scene)
{
	for (unsigned int i = 0; i < scene->num_nodes; i++) {
		setup_node_rendering(scene->nodes[i]);
	}
}

void render_scene(const struct scene_t *scene, const struct shader_t *shader)
{
	for (unsigned int i = 0; i < scene->num_nodes; i++) {
		render_node(scene->nodes[i], shader);
	}
}

void deref_scene(struct scene_t *scene)
{
	if (--scene->num_refs == 0) {
		for (unsigned int i = 0; i < scene->num_nodes; i++) {
			deref_node(scene->nodes[i]);
		}
		free(scene);
	}
}
