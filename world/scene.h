#ifndef WORLD_SCENE_H
#define WORLD_SCENE_H

#include <stdio.h>
#include <stdlib.h>

#include "gltf/scene.h"
#include "shader.h"
#include "world/node.h"
#include "world/lists.h"

struct scene_t {
	struct node_t **nodes;
	unsigned int num_nodes;
	unsigned int num_refs;
};

struct scene_t *load_active_scene(const char *dirname, const char *filename);
struct scene_t *load_scene(const struct gltf_scene_t *gltf_scene);
void link_scene(struct scene_t *scene, const struct gltf_scene_t *gltf_scene,
		struct world_lists_t *lists);
void setup_scene_rendering(struct scene_t *scene);
void render_scene(const struct scene_t *scene, const struct shader_t *shader);
void deref_scene(struct scene_t *scene);

#endif // WORLD_SCENE_H
