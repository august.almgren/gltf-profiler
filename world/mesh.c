#include "world/mesh.h"

struct mesh_t *load_mesh(const struct gltf_mesh_t *gltf_mesh)
{
	struct mesh_t *mesh;
	mesh = malloc(sizeof(*mesh));
	
	mesh->num_primitives = gltf_mesh->num_primitives;
	mesh->primitives = malloc(sizeof(*mesh->primitives) * gltf_mesh->num_primitives);
	for (unsigned int i = 0; i < gltf_mesh->num_primitives; i++) {
		mesh->primitives[i] = load_primitive(gltf_mesh->primitives[i]);
	}

	mesh->num_refs = 1;
	return mesh;
}

void link_mesh(struct mesh_t *mesh, const struct gltf_mesh_t *gltf_mesh,
		struct world_lists_t *lists)
{
	for (unsigned int i = 0; i < gltf_mesh->num_primitives; i++) {
		link_primitive(mesh->primitives[i], gltf_mesh->primitives[i], lists);
	}
}

void setup_mesh_rendering(struct mesh_t *mesh)
{
	for (unsigned int i = 0; i < mesh->num_primitives; i++) {
		setup_primitive_vao(mesh->primitives[i]);
	}
}

void render_mesh(const struct mesh_t *mesh)
{
	for (unsigned int i = 0; i < mesh->num_primitives; i++) {
		render_primitive(mesh->primitives[i]);
	}
}

void deref_mesh(struct mesh_t *mesh)
{
	if (--mesh->num_refs == 0) {
		for (unsigned int i = 0; i < mesh->num_primitives; i++) {
			deref_primitive(mesh->primitives[i]);
		}
		free(mesh->primitives);
		free(mesh);
	}
}
