#ifndef WORLD_LISTS_H
#define WORLD_LISTS_H

// hack to avoid struct undefined compile warnings
struct world_lists_t;

#include <stdio.h>
#include <stdlib.h>

#include "gltf.h"
#include "world/accessor.h"
#include "world/buffer.h"
#include "world/bufferview.h"
#include "world/mesh.h"
#include "world/node.h"
#include "world/scene.h"

struct world_lists_t {
	unsigned int num_accessors;
	struct accessor_t **accessors;
	
	unsigned int num_buffers;
	struct buffer_t **buffers;
	
	unsigned int num_bufferviews;
	struct bufferview_t **bufferviews;
	
	unsigned int num_meshes;
	struct mesh_t **meshes;
	
	unsigned int num_nodes;
	struct node_t **nodes;
	
	unsigned int num_scenes;
	struct scene_t **scenes;

	struct scene_t *scene;
};

struct world_lists_t *load_world_lists(const struct gltf_t *gltf);
void deref_world_lists(struct world_lists_t *lists);

#endif // WORLD_LISTS_H
