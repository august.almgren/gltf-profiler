#ifndef WORLD_NODE_H
#define WORLD_NODE_H

#include <GL/glew.h>
#include <stdlib.h>

#include "gltf/node.h"
#include "shader.h"
#include "world/lists.h"
#include "world/mesh.h"


struct node_t {
	float matrix[16];
	struct mesh_t *mesh;
	unsigned int num_refs;
	unsigned int num_children;
	struct node_t **children;
};

struct node_t *load_node(const struct gltf_node_t *gltf_node);
void link_node(struct node_t *node, const struct gltf_node_t *gltf_node,
		struct world_lists_t *lists);
void setup_node_rendering(struct node_t *node);
void render_node(const struct node_t *node, const struct shader_t *shader);
void deref_node(struct node_t *node);

#endif // WORLD_NODE_H
