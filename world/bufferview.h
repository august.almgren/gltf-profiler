#ifndef WORLD_BUFFERVIEW_H
#define WORLD_BUFFERVIEW_H

#include <GL/glew.h>
#include <stdio.h>
#include <stdlib.h>

#include "gltf/bufferview.h"
#include "world/buffer.h"
#include "world/lists.h"

struct bufferview_t {
	struct buffer_t *buffer;
	unsigned int offset;
	unsigned int len;
	unsigned int stride;
	unsigned int bo;
	unsigned int num_refs;
};

struct bufferview_t *load_bufferview(const struct gltf_bufferview_t *gltf_bufferview);
void link_bufferview(struct bufferview_t *bufferview,
		const struct gltf_bufferview_t *gltf_bufferview,
		struct world_lists_t *lists);
void setup_bufferview_bo(struct bufferview_t *bufferview, const GLuint target);
void deref_bufferview(struct bufferview_t *bufferview);

#endif // WORLD_BUFFERVIEW_H
