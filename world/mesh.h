#ifndef WORLD_MESH_H
#define WORLD_MESH_H

#include <stdio.h>
#include <stdlib.h>

#include "gltf/mesh.h"
#include "world/lists.h"
#include "world/primitive.h"

struct mesh_t {
	unsigned int num_primitives;
	struct primitive_t **primitives;
	unsigned int num_refs;
};

struct mesh_t *load_mesh(const struct gltf_mesh_t *gltf_mesh);
void link_mesh(struct mesh_t *mesh, const struct gltf_mesh_t *gltf_mesh,
		struct world_lists_t *lists);
void setup_mesh_rendering(struct mesh_t *mesh);
void render_mesh(const struct mesh_t *mesh);
void deref_mesh(struct mesh_t *mesh);

#endif // WORLD_MESH_H
