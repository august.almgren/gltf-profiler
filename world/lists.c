#include "world/lists.h"

struct world_lists_t *load_world_lists(const struct gltf_t *gltf)
{
	// load components from gltf

	struct world_lists_t *lists;
	lists = malloc(sizeof(*lists));

	lists->num_accessors = gltf->num_accessors;
	lists->accessors = malloc(sizeof(*lists->accessors) * lists->num_accessors);
	printf("num accessors: %d\n", lists->num_accessors);
	for (unsigned int i = 0; i < lists->num_accessors; i++) {
		lists->accessors[i] = load_accessor(gltf->accessors[i]);
	}

	lists->num_buffers = gltf->num_buffers;
	printf("num buffers: %d\n", lists->num_buffers);
	lists->buffers = malloc(sizeof(*lists->buffers) * lists->num_buffers);
	for (unsigned int i = 0; i < lists->num_buffers; i++) {
		lists->buffers[i] = load_buffer(gltf->buffers[i]);
	}

	lists->num_bufferviews = gltf->num_bufferviews;
	printf("num bufferviews: %d\n", lists->num_bufferviews);
	lists->bufferviews = malloc(sizeof(*lists->bufferviews) * lists->num_bufferviews);
	for (unsigned int i = 0; i < lists->num_bufferviews; i++) {
		lists->bufferviews[i] = load_bufferview(gltf->bufferviews[i]);
	}

	lists->num_meshes = gltf->num_meshes;
	printf("num meshes: %d\n", lists->num_meshes);
	lists->meshes = malloc(sizeof(*lists->meshes) * lists->num_meshes);
	for (unsigned int i = 0; i < lists->num_meshes; i++) {
		lists->meshes[i] = load_mesh(gltf->meshes[i]);
	}

	lists->num_nodes = gltf->num_nodes;
	printf("num nodes: %d\n", lists->num_nodes);
	lists->nodes = malloc(sizeof(*lists->nodes) * lists->num_nodes);
	for (unsigned int i = 0; i < lists->num_nodes; i++) {
		lists->nodes[i] = load_node(gltf->nodes[i]);
	}

	lists->num_scenes = gltf->num_scenes;
	printf("num scenes: %d\n", lists->num_scenes);
	lists->scenes = malloc(sizeof(*lists->scenes) * lists->num_scenes);
	for (unsigned int i = 0; i < lists->num_scenes; i++) {
		lists->scenes[i] = load_scene(gltf->scenes[i]);
	}

	lists->scene = lists->scenes[gltf->scene_id];

	// setup internal linkage

	for (unsigned int i = 0; i < lists->num_accessors; i++) {
		link_accessor(lists->accessors[i], gltf->accessors[i], lists);
	}

	for (unsigned int i = 0; i < lists->num_bufferviews; i++) {
		link_bufferview(lists->bufferviews[i], gltf->bufferviews[i], lists);
	}

	for (unsigned int i = 0; i < lists->num_meshes; i++) {
		link_mesh(lists->meshes[i], gltf->meshes[i], lists);
	}

	for (unsigned int i = 0; i < lists->num_nodes; i++) {
		link_node(lists->nodes[i], gltf->nodes[i], lists);
	}

	for (unsigned int i = 0; i < lists->num_scenes; i++) {
		link_scene(lists->scenes[i], gltf->scenes[i], lists);
	}

	return lists;
}

void deref_world_lists(struct world_lists_t *lists)
{
	for (unsigned int i = 0; i < lists->num_accessors; i++) {
		deref_accessor(lists->accessors[i]);
	}

	for (unsigned int i = 0; i < lists->num_buffers; i++) {
		deref_buffer(lists->buffers[i]);
	}

	for (unsigned int i = 0; i < lists->num_bufferviews; i++) {
		deref_bufferview(lists->bufferviews[i]);
	}

	for (unsigned int i = 0; i < lists->num_meshes; i++) {
		deref_mesh(lists->meshes[i]);
	}

	for (unsigned int i = 0; i < lists->num_nodes; i++) {
		deref_node(lists->nodes[i]);
	}

	for (unsigned int i = 0; i < lists->num_scenes; i++) {
		deref_scene(lists->scenes[i]);
	}

	free(lists);
}
