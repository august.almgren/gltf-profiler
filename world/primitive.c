#include "world/primitive.h"

struct primitive_t *load_primitive(const struct gltf_primitive_t *gltf_primitive)
{
	struct primitive_t *primitive;
	primitive = malloc(sizeof(*primitive));

	primitive->mode = gltf_primitive->mode;
	glGenVertexArrays(1, &primitive->vao);

	return primitive;
}

void link_primitive(struct primitive_t *primitive, const struct gltf_primitive_t *gltf_primitive,
		struct world_lists_t *lists)
{
	if (gltf_primitive->positions.exist) {
		primitive->positions = lists->accessors[gltf_primitive->positions.accessor_id];
		primitive->positions->num_refs++;
	} else {
		primitive->positions = NULL;
	}

	if (gltf_primitive->indices.exist) {
		primitive->indices = lists->accessors[gltf_primitive->indices.accessor_id];
		primitive->indices->num_refs++;
	} else {
		primitive->indices = NULL;
	}
}

void setup_primitive_vao(struct primitive_t *primitive)
{
	glBindVertexArray(primitive->vao);

	setup_accessor_vbo(0, primitive->positions);

	if (primitive->indices) {
		setup_accessor_ebo(primitive->indices);
	}
}

void render_primitive(const struct primitive_t *primitive)
{
	glBindVertexArray(primitive->vao);

	if (primitive->indices) {
		glDrawElements(primitive->mode, primitive->indices->count,
				primitive->indices->component_type, primitive->indices->offset);
	} else {
		glDrawArrays(primitive->mode, 0, primitive->positions->count);
	}

}

void deref_primitive(struct primitive_t *primitive)
{
	// primitives don't have a ref count since they're singletons

	if (primitive->positions) {
		deref_accessor(primitive->positions);
	}

	if (primitive->indices) {
		deref_accessor(primitive->indices);
	}

	free(primitive);
}
