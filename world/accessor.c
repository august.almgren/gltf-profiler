#include "world/accessor.h"

struct accessor_t *load_accessor(const struct gltf_accessor_t *gltf_accessor)
{
	struct accessor_t *accessor;
	accessor = malloc(sizeof(*accessor));

	accessor->offset = gltf_accessor->offset;
	accessor->component_type = gltf_accessor->component_type;
	accessor->component_size = gltf_accessor->component_size;
	accessor->count = gltf_accessor->count;
	accessor->num_refs = 1;

	return accessor;
}

void link_accessor(struct accessor_t *accessor,
		const struct gltf_accessor_t *gltf_accessor, struct world_lists_t *lists)
{
	accessor->bufferview = lists->bufferviews[gltf_accessor->bufferview_id];
	accessor->bufferview->num_refs++;
}

void setup_accessor_vbo(const GLuint attr_index, struct accessor_t *accessor)
{
	setup_bufferview_bo(accessor->bufferview, GL_ARRAY_BUFFER);
	glVertexAttribPointer(attr_index, accessor->component_size, accessor->component_type,
			GL_FALSE, accessor->bufferview->stride, accessor->offset);
	glEnableVertexAttribArray(attr_index);
}

void setup_accessor_ebo(struct accessor_t *accessor)
{
	setup_bufferview_bo(accessor->bufferview, GL_ELEMENT_ARRAY_BUFFER);
}

void deref_accessor(struct accessor_t *accessor)
{
	if (--accessor->num_refs == 0) {
		deref_bufferview(accessor->bufferview);
		free(accessor);
	}
}
