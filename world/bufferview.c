#include "world/bufferview.h"

struct bufferview_t *load_bufferview(const struct gltf_bufferview_t *gltf_bufferview)
{
	struct bufferview_t *bufferview;
	bufferview = malloc(sizeof(*bufferview));

	bufferview->offset = gltf_bufferview->offset;
	bufferview->len = gltf_bufferview->len;
	bufferview->stride = gltf_bufferview->stride;
	bufferview->bo = 0;
	bufferview->num_refs = 1;

	return bufferview;
}

void link_bufferview(struct bufferview_t *bufferview,
		const struct gltf_bufferview_t *gltf_bufferview,
		struct world_lists_t *lists)
{
	bufferview->buffer = lists->buffers[gltf_bufferview->buffer_id];
	bufferview->buffer->num_refs++;
}

void setup_bufferview_bo(struct bufferview_t *bufferview, const GLuint target)
{
	// a single bufferview may have multiple accessors, in which case
	// the buffer object only needs to be uploaded to the gpu once
	if (bufferview->bo == 0) {
		glGenBuffers(1, &bufferview->bo);
		glBindBuffer(target, bufferview->bo);
		glBufferData(target, bufferview->len,
				bufferview->buffer->data + bufferview->offset, GL_STATIC_DRAW);
	} else {
		glBindBuffer(target, bufferview->bo);
	}
}

void deref_bufferview(struct bufferview_t *bufferview)
{
	if (--bufferview->num_refs == 0) {
		deref_buffer(bufferview->buffer);
		free(bufferview);
	}
}
