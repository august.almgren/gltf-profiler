#ifndef SHADER_H
#define SHADER_H

#include <GL/glew.h> // include before other gl libs
#include <GLFW/glfw3.h>
#include <stdio.h>
#include <stdlib.h>

struct shader_t {
	GLuint id;
	struct {
		GLuint curr_time;
		GLuint model;
	} uniforms;
};

struct shader_t *load_shader(const char *vert_fpath, const char *frag_fpath);
void free_shader(struct shader_t *shader);

#endif // SHADER_H
