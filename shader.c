#include "shader.h"

static unsigned int load_shader_id(const char *fpath, GLenum type)
{
	// load from fs

	FILE *f = fopen(fpath, "r");
	if (!f) {
		printf("couldn't open file %s\n", fpath);
		return 0;
	}

	fseek(f, 0L, SEEK_END);
	const size_t fsize = ftell(f);
	rewind(f);

	GLchar *src = malloc(fsize);
	fread(src, fsize, 1, f);
	fclose(f);

	// create & compile

	GLuint id = glCreateShader(type);
	if (!id) {
		printf("couldn't create shader\n");
		free(src);
		return 0;
	}

	glShaderSource(id, 1, &src, NULL);
	glCompileShader(id);
	free(src);

	// validate & return

	GLint success;
	glGetShaderiv(id, GL_COMPILE_STATUS, &success);
	if (!success) {
		printf("couldn't compile shader %s\n", fpath);
		GLchar infolog[512];
		glGetShaderInfoLog(id, 512, NULL, infolog);
		printf("shader error: %s\n", infolog);
		return 0;
	}

	return id;
}

struct shader_t *load_shader(const char *vert_fpath, const char *frag_fpath)
{
	// compile

	GLuint vshader = load_shader_id(vert_fpath, GL_VERTEX_SHADER);
	if (!vshader) {
		return NULL;
	}

	GLuint fshader = load_shader_id(frag_fpath, GL_FRAGMENT_SHADER);
	if (!fshader) {
		glDeleteShader(vshader);
		return NULL;
	}

	// link

	struct shader_t *shader;
	shader = malloc(sizeof(*shader));

	shader->id = glCreateProgram();
	glAttachShader(shader->id, vshader);
	glAttachShader(shader->id, fshader);
	glLinkProgram(shader->id);
	glDetachShader(shader->id, vshader);
	glDetachShader(shader->id, fshader);
	glDeleteShader(vshader);
	glDeleteShader(fshader);

	// validate

	GLint success;
	glGetProgramiv(shader->id, GL_LINK_STATUS, &success);
	if (!success) {
		printf("couldn't link shader\n");
		GLchar infolog[512];
		glGetProgramInfoLog(shader->id, 512, NULL, infolog);
		printf("shader error: %s\n", infolog);
		free(shader);
		return NULL;
	}

	// setup uniform & activate

	shader->uniforms.curr_time = glGetUniformLocation(shader->id, "curr_time");
	shader->uniforms.model = glGetUniformLocation(shader->id, "model");

	return shader;
}

void free_shader(struct shader_t *shader)
{
	glDeleteProgram(shader->id);
	free(shader);
}
