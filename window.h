#ifndef WINDOW_H
#define WINDOW_H

#include <GLFW/glfw3.h>
#include <stdbool.h>
#include <stdio.h>

#include "settings.h"

GLFWwindow *init_window();
void toggle_fps_cap();
void clear_window();

#endif // WINDOW_H
