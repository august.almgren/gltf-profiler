#version 330 core

layout (location = 0) in vec3 position;

uniform float curr_time = 0.0;

mat4 translate(vec3 v) {
	return mat4(
		1.0, 0.0, 0.0, 0.0,
		0.0, 1.0, 0.0, 0.0,
		0.0, 0.0, 1.0, 0.0,
		v.x, v.y, v.z, 1.0
	);
}

mat4 rotate_x(float angle) {
	float x = cos(angle);
	float y = sin(angle);
	return mat4(
		1.0, 0.0, 0.0, 0.0,
		0.0,   x,   y, 0.0,
		0.0,  -y,   x, 0.0,
		0.0, 0.0, 0.0, 1.0
	);
}

mat4 rotate_y(float angle) {
	float x = cos(angle);
	float y = sin(angle);
	return mat4(
		  x, 0.0,   y, 0.0,
		0.0, 1.0, 0.0, 0.0,
		 -y, 0.0,   x, 0.0,
		0.0, 0.0, 0.0, 1.0
	);
}

mat4 rotate_z(float angle) {
	float x = cos(angle);
	float y = sin(angle);
	return mat4(
		  x,   y, 0.0, 0.0,
		 -y,   x, 0.0, 0.0,
		0.0, 0.0, 1.0, 0.0,
		0.0, 0.0, 0.0, 1.0
	);
}

mat4 scale(vec3 v) {
	return mat4(
		v.x, 0.0, 0.0, 0.0,
		0.0, v.y, 0.0, 0.0,
		0.0, 0.0, v.z, 0.0,
		0.0, 0.0, 0.0, 1.0
	);
}

void main() {

	// box
	//mat4 model = rotate_y(curr_time);
	//mat4 view = rotate_x(radians(45.0));

	// sponza
	//mat4 model = rotate_y(curr_time) * scale(vec3(0.0004, 0.0004, 0.0004));
	//mat4 view = translate(vec3(0.0, -0.2, 0.0)) * rotate_x(radians(60.0));

	// suzanne
	mat4 model = rotate_y(curr_time) * scale(vec3(0.5, 0.5, 0.5));
	mat4 view = rotate_x(radians(45.0));

	gl_Position = view * model * vec4(position, 1.0);
}
