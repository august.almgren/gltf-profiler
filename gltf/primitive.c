#include "gltf/primitive.h"

struct gltf_primitive_t *load_gltf_primitive(const json_value *obj)
{
	struct gltf_primitive_t *primitive;
	primitive = malloc(sizeof(*primitive));

	primitive->positions.exist = false;
	primitive->indices.exist = false;

	const unsigned int len = json_objlen(obj);
	for (unsigned int i = 0; i < len; i++) {

		if (json_isobjname(obj, i, "attributes")) {
			const json_value *attrs = json_objval(obj, i);
			const unsigned int num_attrs = json_objlen(attrs);
			for (unsigned int j = 0; j < num_attrs; j++) {
				if (json_isobjname(attrs, j, "POSITION")) {
					primitive->positions.exist = true;
					primitive->positions.accessor_id = json_objint(attrs, j);
				}
			}
		} else if (json_isobjname(obj, i, "indices")) {
			primitive->indices.exist = true;
			primitive->indices.accessor_id = json_objint(obj, i);
		} else if (json_isobjname(obj, i, "mode")) {
			primitive->mode = json_objint(obj, i);
		}
	}

	return primitive;
}

struct gltf_primitive_t **load_gltf_primitives(const json_value *arr)
{
	const unsigned int len = json_arrlen(arr);

	struct gltf_primitive_t **primitives;
	primitives = malloc(len * sizeof(*primitives));

	for (unsigned int i = 0; i < len; i++) {
		primitives[i] = load_gltf_primitive(json_arrval(arr, i));
	}

	return primitives;
}

void free_gltf_primitives(struct gltf_primitive_t **primitives, const unsigned int num_primitives)
{
	for (unsigned int i = 0; i < num_primitives; i++) {
		free(primitives[i]);
	}

	free(primitives);
}
