#include "gltf/buffer.h"

static struct gltf_buffer_t *load_gltf_buffer(const json_value *obj, const char *fdir)
{
	unsigned int len = json_objlen(obj);

	struct gltf_buffer_t *buffer;
	buffer = malloc(sizeof(*buffer));

	for (unsigned int i = 0; i < len; i++) {
		if (json_isobjname(obj, i, "uri")) {
			const char *fname = json_objstr(obj, i);
			buffer->fpath = malloc(strlen(fdir) + strlen(fname) + 1);
			strcpy(buffer->fpath, fdir);
			strcat(buffer->fpath, fname);
		} else if (json_isobjname(obj, i, "byteLength")) {
			buffer->len = json_objint(obj, i);
		}
	}

	return buffer;
}

struct gltf_buffer_t **load_gltf_buffers(const json_value *arr, const char *fdir)
{
	const unsigned int len = json_arrlen(arr);

	struct gltf_buffer_t **buffers;
	buffers = malloc(len * sizeof(*buffers));

	for (unsigned int i = 0; i < len; i++) {
		buffers[i] = load_gltf_buffer(json_arrval(arr, i), fdir);
	}

	return buffers;
}

void free_gltf_buffers(struct gltf_buffer_t **buffers, const unsigned int num_buffers)
{
	for (unsigned int i = 0; i < num_buffers; i++) {
		free(buffers[i]->fpath);
		free(buffers[i]);
	}

	free(buffers);
}
