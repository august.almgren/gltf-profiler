#ifndef GLTF_MESH_H
#define GLTF_MESH_H

#include <stdbool.h>
#include <stdlib.h>

#include "json.h"
#include "lib/json-parser/json.h"
#include "gltf/primitive.h"

struct gltf_mesh_t {
	unsigned int num_primitives;
	struct gltf_primitive_t **primitives;
};

struct gltf_mesh_t **load_gltf_meshes(const json_value *arr);
void free_gltf_meshes(struct gltf_mesh_t **meshes, const unsigned int num_meshes);

#endif // GLTF_MESH_H
