#ifndef GLTF_BUFFERVIEW_H
#define GLTF_BUFFERVIEW_H

#include <stdlib.h>

#include "lib/json-parser/json.h"
#include "json.h"

struct gltf_bufferview_t {
	unsigned int buffer_id;
	unsigned int len;
	unsigned int offset;
	unsigned int stride;
};

struct gltf_bufferview_t **load_gltf_bufferviews(const json_value *arr);
void free_gltf_bufferviews(struct gltf_bufferview_t **bufferviews, unsigned int num_bufferviews);

#endif // GLTF_BUFFERVIEW_H
