#ifndef GLTF_SCENE_H
#define GLTF_SCENE_H

#include <stdlib.h>

#include "lib/json-parser/json.h"
#include "json.h"

struct gltf_scene_t {
	unsigned int num_nodes;
	unsigned int *node_ids;
};

struct gltf_scene_t **load_gltf_scenes(const json_value *arr);
void free_gltf_scenes(struct gltf_scene_t **scenes, const unsigned int num_scenes);

#endif // GLTF_SCENE_H
