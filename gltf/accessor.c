#include "gltf/accessor.h"

static struct gltf_accessor_t *load_gltf_accessor(const json_value *obj)
{
	const unsigned int len = json_objlen(obj);

	struct gltf_accessor_t *accessor;
	accessor = malloc(sizeof(*accessor));

	for (unsigned int i = 0; i < len; i++) {

		if (json_isobjname(obj, i, "bufferView")) {
			accessor->bufferview_id = json_objint(obj, i);
		} else if (json_isobjname(obj, i, "byteOffset")) {
			accessor->offset = json_objint(obj, i);
		} else if (json_isobjname(obj, i, "componentType")) {
			accessor->component_type = json_objint(obj, i);
		} else if (json_isobjname(obj, i, "type")) {
			const char *container_name = json_objstr(obj, i);
			if (strcmp(container_name, "SCALAR") == 0) {
				accessor->component_size = 1;
			} else if (strcmp(container_name, "VEC2") == 0) {
				accessor->component_size = 2;
			} else if (strcmp(container_name, "VEC3") == 0) {
				accessor->component_size = 3;
			} else if (strcmp(container_name, "VEC4") == 0) {
				accessor->component_size = 4;
			} else {
				accessor->component_size = 0;
			}
		} else if (json_isobjname(obj, i, "count")) {
			accessor->count = json_objint(obj, i);
		}
	}

	return accessor;
}

struct gltf_accessor_t **load_gltf_accessors(const json_value *arr)
{
	const unsigned int num_accessors = json_arrlen(arr);

	struct gltf_accessor_t **accessors;
	accessors = malloc(num_accessors * sizeof(*accessors));

	for (unsigned int i = 0; i < num_accessors; i++) {
		accessors[i] = load_gltf_accessor(json_arrval(arr, i));
	}

	return accessors;
}

void free_gltf_accessors(struct gltf_accessor_t **accessors, const unsigned int num_accessors)
{
	for (unsigned int i = 0; i < num_accessors; i++) {
		free(accessors[i]);
	}

	free(accessors);
}
