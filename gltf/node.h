#ifndef GLTF_NODE_H
#define GLTF_NODE_H

#include <stdbool.h>
#include <stdlib.h>

#include "lib/json-parser/json.h"
#include "json.h"

struct gltf_node_t {
	float matrix[16];
	struct {
		bool exists;
		unsigned int accessor_id;
	} mesh;
	unsigned int num_children;
	unsigned int *child_ids;
};

struct gltf_node_t **load_gltf_nodes(const json_value *arr);
void free_gltf_nodes(struct gltf_node_t **nodes, const unsigned int num_nodes);

#endif // GLTF_NODE_H
