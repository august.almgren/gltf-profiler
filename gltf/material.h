#ifndef TYPE_GLTF_MATERIAL_H
#define TYPE_GLTF_MATERIAL_H

#include <stdbool.h>
#include <string.h>
#include "sys/mem.h"
#include "type/json.h"
#include "type/prim/array.h"

struct gltf_material_texinfo_t {

	unsigned int index;
	unsigned int texcoord;
	float scale;
};

struct gltf_material_pbr_t {

	float base_color_factor[4];
	struct gltf_material_texinfo_t *base_color_texture;

	float metallic_factor;
	float roughness_factor;
	struct gltf_material_texinfo_t *metallic_roughness_texture;
};

struct gltf_material_t {

	char *alpha_mode;
	bool double_sided;

	struct gltf_material_pbr_t *pbr_metallic_roughness;

	struct gltf_material_texinfo_t *normal_texture;
	float emissive_factor[3];
};

struct gltf_material_t **load_gltf_materials(const json_value *arr);
void free_gltf_materials(struct gltf_material_t **arr, const unsigned int len);

#endif // TYPE_GLTF_MATERIAL_H
