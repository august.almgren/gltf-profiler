#include "gltf/bufferview.h"

static struct gltf_bufferview_t *load_gltf_bufferview(const json_value *obj)
{
	unsigned int len = json_objlen(obj);

	struct gltf_bufferview_t *bufferview;
	bufferview = malloc(sizeof(*bufferview));

	bufferview->stride = 0;

	for (unsigned int i = 0; i < len; i++) {
		if (json_isobjname(obj, i, "buffer")) {
			bufferview->buffer_id = json_objint(obj, i);
		} else if (json_isobjname(obj, i, "byteLength")) {
			bufferview->len = json_objint(obj, i);
		} else if (json_isobjname(obj, i, "byteOffset")) {
			bufferview->offset = json_objint(obj, i);
		} else if (json_isobjname(obj, i, "byteStride")) {
			bufferview->stride = json_objint(obj, i);
		}
	}

	return bufferview;
}

struct gltf_bufferview_t **load_gltf_bufferviews(const json_value *arr)
{
	const unsigned int len = json_arrlen(arr);

	struct gltf_bufferview_t **bufferviews;
	bufferviews = malloc(len * sizeof(*bufferviews));

	for (unsigned int i = 0; i < len; i++)
		bufferviews[i] = load_gltf_bufferview(json_arrval(arr, i));

	return bufferviews;
}

void free_gltf_bufferviews(struct gltf_bufferview_t **bufferviews,
		const unsigned int num_bufferviews)
{
	for (unsigned int i = 0; i < num_bufferviews; i++) {
		free(bufferviews[i]);
	}

	free(bufferviews);
}
