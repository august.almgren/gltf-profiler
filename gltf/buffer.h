#ifndef GLTF_BUFFER_H
#define GLTF_BUFFER_H

#include <string.h>
#include <stdlib.h>

#include "json.h"
#include "lib/json-parser/json.h"

struct gltf_buffer_t {
	char *fpath;
	unsigned int len;
};

struct gltf_buffer_t **load_gltf_buffers(const json_value *arr, const char *fdir);
void free_gltf_buffers(struct gltf_buffer_t **buffers, const unsigned int num_buffers);

#endif // GLTF_BUFFER_H
