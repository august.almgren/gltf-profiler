#include "gltf/node.h"

static struct gltf_node_t *load_gltf_node(const json_value *obj)
{
	struct gltf_node_t *node;
	node = malloc(sizeof(*node));

	node->mesh.exists = false;

	node->matrix[ 0] = 1.0f;
	node->matrix[ 1] = 0.0f;
	node->matrix[ 2] = 0.0f;
	node->matrix[ 3] = 0.0f;

	node->matrix[ 4] = 0.0f;
	node->matrix[ 5] = 1.0f;
	node->matrix[ 6] = 0.0f;
	node->matrix[ 7] = 0.0f;

	node->matrix[ 8] = 0.0f;
	node->matrix[ 9] = 0.0f;
	node->matrix[10] = 1.0f;
	node->matrix[11] = 0.0f;

	node->matrix[12] = 0.0f;
	node->matrix[13] = 0.0f;
	node->matrix[14] = 0.0f;
	node->matrix[15] = 1.0f;

	node->num_children = 0;
	node->child_ids = NULL;

	const unsigned int len = json_objlen(obj);
	for (unsigned int i = 0; i < len; i++) {
		if (json_isobjname(obj, i, "matrix")) {
			double *m = json_objdblarr(obj, i);
			for (unsigned int j = 0; j < 16; j++) {
				node->matrix[j] = (float)m[j];
			}
			free(m);
		} else if (json_isobjname(obj, i, "mesh")) {
			node->mesh.exists = true;
			node->mesh.accessor_id = json_objint(obj, i);
		} else if (json_isobjname(obj, i, "children")) {
			const json_value *arr = json_objval(obj, i);
			node->num_children = json_arrlen(arr);
			node->child_ids = malloc(node->num_children * sizeof(*node->child_ids));
			for (unsigned int j = 0; j < node->num_children; j++) {
				node->child_ids[j] = json_arrint(arr, j);
			}
		}
	}

	return node;
}

struct gltf_node_t **load_gltf_nodes(const json_value *arr)
{
	const unsigned int len = json_arrlen(arr);

	struct gltf_node_t **nodes;
	nodes = malloc(len * sizeof(*nodes));

	for (unsigned int i = 0; i < len; i++) {
		nodes[i] = load_gltf_node(json_arrval(arr, i));
	}

	return nodes;
}

void free_gltf_nodes(struct gltf_node_t **nodes, unsigned int num_nodes)
{
	for (unsigned int i = 0; i < num_nodes; i++) {
		if (nodes[i]->child_ids) {
			free(nodes[i]->child_ids);
		}

		free(nodes[i]);
	}

	free(nodes);
}
