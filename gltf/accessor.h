#ifndef GLTF_ACCESSOR_H
#define GLTF_ACCESSOR_H

#include <string.h>
#include <stdlib.h>

#include "lib/json-parser/json.h"
#include "json.h"

struct gltf_accessor_t {
	unsigned int bufferview_id;
	unsigned int offset;
	unsigned int component_type;
	unsigned int component_size;
	unsigned int count;
};

struct gltf_accessor_t **load_gltf_accessors(const json_value *arr);
void free_gltf_accessors(struct gltf_accessor_t **accessors, unsigned int num_accessors);

#endif // GLTF_ACCESSOR_H
