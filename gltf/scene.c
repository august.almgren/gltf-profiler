#include "gltf/scene.h"

static struct gltf_scene_t *load_gltf_scene(const json_value *obj)
{
	struct gltf_scene_t *scene;
	scene = malloc(sizeof(*scene));

	const json_value *arr = json_objval(obj, 0);
	scene->num_nodes = json_arrlen(arr);
	scene->node_ids = malloc(scene->num_nodes * sizeof(*scene->node_ids));
	for (unsigned int i = 0; i < scene->num_nodes; i++) {
		scene->node_ids[i] = json_arrint(arr, i);
	}

	return scene;
}

struct gltf_scene_t **load_gltf_scenes(const json_value *arr)
{
	const unsigned int len = json_arrlen(arr);

	struct gltf_scene_t **scenes;
	scenes = malloc(len * sizeof(*scenes));

	for (unsigned int i = 0; i < len; i++) {
		scenes[i] = load_gltf_scene(json_arrval(arr, i));
	}

	return scenes;
}

void free_gltf_scenes(struct gltf_scene_t **scenes, const unsigned int num_scenes)
{
	for (unsigned int i = 0; i < num_scenes; i++) {
		free(scenes[i]->node_ids);
		free(scenes[i]);
	}

	free(scenes);
}
