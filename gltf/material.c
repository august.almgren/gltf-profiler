#include "type/gltf/material.h"

static struct gltf_material_texinfo_t *_load_texinfo(const json_value *obj)
{
	const unsigned int len = json_objlen(obj);

	struct gltf_material_texinfo_t *texinfo;
	texinfo = malloc(sizeof(*texinfo));

	for (unsigned int i = 0; i < len; i++) {

		if (json_isobjname(obj, i, "index")) {
			texinfo->index = json_objint(obj, i);
		} else if (json_isobjname(obj, i, "texcoord")) {
			texinfo->texcoord = json_objint(obj, i);
		} else if (json_isobjname(obj, i, "scale")) {
			switch (obj->type) {
			case json_integer:
				texinfo->scale = json_objint(obj, i);
				break;
			case json_double:
				texinfo->scale = json_objdbl(obj, i);
				break;
			default:
				break;
			}
		}
	}

	return texinfo;
}

static void _free_texinfo(struct gltf_material_texinfo_t *texinfo)
{
	free(texinfo);
}

static struct gltf_material_pbr_t *_load_pbr(const json_value *obj)
{
	const unsigned int len = json_objlen(obj);

	struct gltf_material_pbr_t *pbr;
	pbr = malloc(sizeof(*pbr));

	for (unsigned int i = 0; i < len; i++) {

		if (json_isobjname(obj, i, "baseColorFactor")) {
			const vec4f v = json_objvec4f(obj, i);
			memcpy(&pbr->base_color_factor, &v, sizeof(v));
		} else if (json_isobjname(obj, i, "baseColorTexture")) {
			pbr->base_color_texture = _load_texinfo(json_objval(obj, i));
		} else if (json_isobjname(obj, i, "metallicFactor")) {
			pbr->metallic_factor = json_objdbl(obj, i);
		} else if (json_isobjname(obj, i, "roughnessFactor")) {
			pbr->roughness_factor = json_objdbl(obj, i);
		} else if (json_isobjname(obj, i, "materialRoughnessTexture")) {
			pbr->metallic_roughness_texture = _load_texinfo(json_objval(obj, i));
		}
	}

	return pbr;
}

static void _free_pbr(struct gltf_material_pbr_t *pbr)
{
	if (!pbr)
		return;

	_free_texinfo(pbr->base_color_texture);
	_free_texinfo(pbr->metallic_roughness_texture);

	free(pbr);
}

struct gltf_material_t *_load_material(const json_value *obj)
{
	const unsigned int len = json_objlen(obj);

	struct gltf_material_t *material;
	material = malloc(sizeof(*material));

	material->alpha_mode = NULL;
	material->pbr_metallic_roughness = NULL;
	material->normal_texture = NULL;

	for (unsigned int i = 0; i < len; i++) {

		if (json_isobjname(obj, i, "alphaMode")) {
			material->alpha_mode = strdup(json_objstr(obj, i));
		} else if (json_isobjname(obj, i, "doubleSided")) {
			material->double_sided = json_objbool(obj, i);
		} else if (json_isobjname(obj, i, "pbrMetallicRoughness")) {
			material->pbr_metallic_roughness = _load_pbr(json_objval(obj, i));
		} else if (json_isobjname(obj, i, "normalTexture")) {
			material->normal_texture = _load_texinfo(json_objval(obj, i));
		} else if (json_isobjname(obj, i, "emissiveFactor")) {
			const vec3f v = json_objvec3f(obj, i);
			memcpy(&material->emissive_factor, &v, sizeof(v));
		}
	}

	return material;
}

struct gltf_material_t **load_gltf_materials(const json_value *arr)
{
	const unsigned int len = json_arrlen(arr);

	struct gltf_material_t **materials;
	materials = malloc(len * sizeof(*materials));

	for (unsigned int i = 0; i < len; i++)
		materials[i] = _load_material(json_arrval(arr, i));

	return materials;
}

static void _free_material(struct gltf_material_t *material)
{
	if (!material)
		return;

	free(material->alpha_mode);
	_free_pbr(material->pbr_metallic_roughness);
	_free_texinfo(material->normal_texture);

	free(material);
}

void free_gltf_materials(struct gltf_material_t **arr, unsigned int len)
{
	if (!arr)
		return;

	for (unsigned int i = 0; i < len; i++)
		_free_material(arr[i]);

	free(arr);
}
