#ifndef GLTF_PRIMITIVE_H
#define GLTF_PRIMITIVE_H

#include <stdbool.h>
#include <stdlib.h>

#include "lib/json-parser/json.h"
#include "json.h"

struct gltf_primitive_t {
	struct {
		bool exist;
		unsigned int accessor_id;
	} positions, indices;
	unsigned int mode;
};

struct gltf_primitive_t **load_gltf_primitives(const json_value *arr);
void free_gltf_primitives(struct gltf_primitive_t **primitives,
		const unsigned int num_primitives);

#endif // GLTF_PRIMITIVE_H
