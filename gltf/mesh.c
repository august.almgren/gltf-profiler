#include "gltf/mesh.h"

struct gltf_mesh_t *load_gltf_mesh(const json_value *obj)
{
	struct gltf_mesh_t *mesh;
	mesh = malloc(sizeof(*mesh));
	mesh->num_primitives = 0;

	const unsigned int len = json_objlen(obj);
	for (unsigned int i = 0; i < len; i++) {
		if (json_isobjname(obj, i, "primitives")) {
			const json_value *primitives = json_objval(obj, i);
			mesh->num_primitives = json_arrlen(primitives);
			mesh->primitives = load_gltf_primitives(primitives);
		}
	}

	return mesh;
}

struct gltf_mesh_t **load_gltf_meshes(const json_value *arr)
{
	const unsigned int len = json_arrlen(arr);

	struct gltf_mesh_t **meshes;
	meshes = malloc(len * sizeof(*meshes));

	for (unsigned int i = 0; i < len; i++) {
		meshes[i] = load_gltf_mesh(json_arrval(arr, i));
	}

	return meshes;
}

void free_gltf_meshes(struct gltf_mesh_t **meshes, const unsigned int num_meshes)
{
	for (unsigned int i = 0; i < num_meshes; i++) {
		free_gltf_primitives(meshes[i]->primitives, meshes[i]->num_primitives);
		free(meshes[i]);
	}

	free(meshes);
}
