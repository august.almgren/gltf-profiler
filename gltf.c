#include "gltf.h"

struct gltf_t *load_gltf(const char *fdir, const char *fname)
{
	json_value *json = load_json(fdir, fname);
	if (!json) {
		return NULL;
	}
	printf("json load complete\n");

	struct gltf_t *gltf;
	gltf = malloc(sizeof(*gltf));

	for (unsigned int i = 0; i < json_objlen(json); i++) {

		const json_value *value = json_objval(json, i);

		if (json_isobjname(json, i, "accessors")) {
			gltf->num_accessors = json_arrlen(value);
			gltf->accessors = load_gltf_accessors(value);
		} else if (json_isobjname(json, i, "buffers")) {
			gltf->num_buffers = json_arrlen(value);
			gltf->buffers = load_gltf_buffers(value, fdir);
		} else if (json_isobjname(json, i, "bufferViews")) {
			gltf->num_bufferviews = json_arrlen(value);
			gltf->bufferviews = load_gltf_bufferviews(value);
		} else if (json_isobjname(json, i, "meshes")) {
			gltf->num_meshes = json_arrlen(value);
			gltf->meshes = load_gltf_meshes(value);
		} else if (json_isobjname(json, i, "nodes")) {
			gltf->num_nodes = json_arrlen(value);
			gltf->nodes = load_gltf_nodes(value);
		} else if (json_isobjname(json, i, "scene")) {
			gltf->scene_id = json_int(value);
		} else if (json_isobjname(json, i, "scenes")) {
			gltf->num_scenes = json_arrlen(value);
			gltf->scenes = load_gltf_scenes(value);
		}
	}

	// TODO validate gltf here

	free_json(json);

	return gltf;
}

void free_gltf(struct gltf_t *gltf)
{
	free_gltf_accessors(gltf->accessors, gltf->num_accessors);
	free_gltf_buffers(gltf->buffers, gltf->num_buffers);
	free_gltf_bufferviews(gltf->bufferviews, gltf->num_bufferviews);
	free_gltf_meshes(gltf->meshes, gltf->num_meshes);
	free_gltf_nodes(gltf->nodes, gltf->num_nodes);
	free_gltf_scenes(gltf->scenes, gltf->num_scenes);

	free(gltf);
}
