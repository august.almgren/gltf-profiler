#include "json.h"

json_value *load_json(const char *fdir, const char *fname)
{
	char *fpath = malloc(strlen(fdir) + strlen(fname) + 1);
	strcpy(fpath, fdir);
	strcat(fpath, fname);

	FILE *filestream = fopen(fpath, "r");
	if (!filestream) {
		printf("couldn't open %s\n", fpath);
		free(fpath);
		return NULL;
	}
	free(fpath);

	fseek(filestream, 0, SEEK_END);
	size_t filesize = ftell(filestream);
	rewind(filestream);

	char *filedata = malloc(filesize);
	if (!filedata) {
		printf("couldn't allocate memory for %s\n", fpath);
		fclose(filestream);
		return NULL;
	}

	if (fread(filedata, filesize, 1, filestream) != 1) {
		printf("couldn't read %s\n", fpath);
		free(filedata);
		fclose(filestream);
		return NULL;
	}
	fclose(filestream);

	json_settings settings = {.settings = json_enable_comments};
	char *json_err_msg = malloc(256);
	json_value *json = json_parse_ex(&settings, filedata, filesize, json_err_msg);

	if (!json) {
		printf("couldn't parse json from %s\n", fpath);
		free(filedata);
		printf("json-parser: %s\n", json_err_msg);
		free(json_err_msg);
		return NULL;
	}
	free(filedata);
	free(json_err_msg);

	return json;
}

void inline free_json(json_value *json)
{
	json_value_free(json);
}

// util functions to simplify json data retrieval

// primitives

inline unsigned int json_int(const json_value *v)
{
	return v->u.integer;
}

inline double json_dbl(const json_value *v)
{
	return v->u.dbl;
}

inline bool json_bool(const json_value *v)
{
	return v->u.boolean;
}

// NOTE: remember to free returned arrays

inline unsigned int *json_intarr(const json_value *v)
{
	const unsigned int len = json_arrlen(v);
	unsigned int *arr;
	arr = malloc(len * sizeof(*arr));
	for (unsigned int i = 0; i < len; i++) {
		arr[i] = json_arrint(v, i);
	}
	return arr;
}

inline double *json_dblarr(const json_value *v)
{
	const unsigned int len = json_arrlen(v);
	double *arr;
	arr = malloc(len * sizeof(*arr));
	for (unsigned int i = 0; i < len; i++) {
		arr[i] = json_arrdbl(v, i);
	}
	return arr;
}

inline bool *json_boolarr(const json_value *v)
{
	const unsigned int len = json_arrlen(v);
	bool *arr;
	arr = malloc(len * sizeof(*arr));
	for (unsigned int i = 0; i < len; i++) {
		arr[i] = json_arrbool(v, i);
	}
	return arr;
}

// NOTE remember to copy strings before getting rid of json

inline char *json_str(const json_value *v)
{
	return v->u.string.ptr;
}

inline unsigned int json_strlen(const json_value *v)
{
	return v->u.string.length;
}

// array primitives

inline unsigned int json_arrlen(const json_value *v)
{
	return v->u.array.length;
}

inline json_value *json_arrval(const json_value *v, const unsigned int i)
{
	return v->u.array.values[i];
}

inline unsigned int json_arrint(const json_value *v, const unsigned int i)
{
	return json_int(json_arrval(v, i));
}

inline double json_arrdbl(const json_value *v, const unsigned int i)
{
	return json_dbl(json_arrval(v, i));
}

inline bool json_arrbool(const json_value *v, const unsigned int i)
{
	return json_bool(json_arrval(v, i));
}

// NOTE: remember to free returned arrays

inline unsigned int *json_arrintarr(const json_value *v, const unsigned int i)
{
	return json_intarr(json_arrval(v, i));
}

inline double *json_arrdblarr(const json_value *v, const unsigned int i)
{
	return json_dblarr(json_arrval(v, i));
}

inline bool *json_arrboolarr(const json_value *v, const unsigned int i)
{
	return json_boolarr(json_arrval(v, i));
}

// NOTE remember to copy strings before getting rid of json

inline char *json_arrstr(const json_value *v, const unsigned int i)
{
	return json_str(json_arrval(v, i));
}

inline unsigned int json_arrstrlen(const json_value *v, const unsigned int i)
{
	return json_strlen(json_arrval(v, i));
}

// object primitives

inline unsigned int json_objlen(const json_value *v)
{
	return v->u.object.length;
}

inline char *json_objname(const json_value *v, const unsigned int i)
{
	return v->u.object.values[i].name;
}

inline bool json_isobjname(const json_value *v, const unsigned int i, const char *s)
{
	return strcmp(json_objname(v, i), s) == 0;
}

inline json_value *json_objval(const json_value *v, const unsigned int i)
{
	return v->u.object.values[i].value;
}

inline unsigned int json_objint(const json_value *v, const unsigned int i)
{
	return json_int(json_objval(v, i));
}

inline double json_objdbl(const json_value *v, const unsigned int i)
{
	return json_dbl(json_objval(v, i));
}

inline bool json_objbool(const json_value *v, const unsigned int i)
{
	return json_bool(json_objval(v, i));
}

// NOTE: remember to free returned arrays

inline unsigned int *json_objintarr(const json_value *v, const unsigned int i)
{
	return json_intarr(json_objval(v, i));
}

inline double *json_objdblarr(const json_value *v, const unsigned int i)
{
	return json_dblarr(json_objval(v, i));
}

inline bool *json_objboolarr(const json_value *v, const unsigned int i)
{
	return json_boolarr(json_objval(v, i));
}

// NOTE remember to copy strings before getting rid of json

inline char *json_objstr(const json_value *v, const unsigned int i)
{
	return json_str(json_objval(v, i));
}

inline unsigned int json_objstrlen(const json_value *v, const unsigned int i)
{
	return json_strlen(json_objval(v, i));
}
