CC = clang
CFLAGS =  -I. 
LDLIBS = -lglfw -lGLEW -lGLU -lGL -lm

MAIN_OBJS = main.o settings.o
JSON_OBJS = json.o lib/json-parser/json.o
GLTF_OBJS = gltf.o gltf/accessor.o gltf/buffer.o gltf/bufferview.o gltf/mesh.o gltf/node.o \
	    gltf/primitive.o gltf/scene.o
RENDER_OBJS = shader.o window.o
WORLD_OBJS = world/accessor.o world/buffer.o world/bufferview.o world/lists.o \
	     world/mesh.o world/node.o world/primitive.o world/scene.o
OBJS = $(MAIN_OBJS) $(JSON_OBJS) $(GLTF_OBJS) $(RENDER_OBJS) $(WORLD_OBJS)

TARGET_DEBUG = gltf-profiler-debug
TARGET_RELEASE = gltf-profiler-release
TARGETS = $(TARGET_DEBUG) $(TARGET_RELEASE)

all: release

debug: TARGET = $(TARGET_DEBUG)
debug: CFLAGS += -O0 -DDEBUG -g3 -pg -DJSON_TRACK_SOURCE
debug: _build

release: TARGET = $(TARGET_RELEASE)
release: CFLAGS += -O3 -DNDEBUG
release: _build

_build: $(OBJS)
	$(CC) $(LDLIBS) $(OBJS) -o $(TARGET)

main.o: gltf.h settings.h world/scene.h shader.h window.h
settings.h:

lib/json-parser/json.o:
json.o: lib/json-parser/json.h

gltf.o: lib/json-parser/json.h json.h gltf/accessor.h gltf/buffer.h gltf/bufferview.h \
	gltf/mesh.h gltf/node.h gltf/scene.h
gltf/accessor.o: lib/json-parser/json.h json.h
gltf/buffer.o: lib/json-parser/json.h json.h
gltf/bufferview.o: lib/json-parser/json.h json.h
gltf/mesh.o: lib/json-parser/json.h json.h gltf/primitive.h
gltf/primitive.o: lib/json-parser/json.h json.h
gltf/node.o: lib/json-parser/json.h json.h
gltf/scene.o: lib/json-parser/json.h json.h

shader.h: 
window.h: settings.o

world/accessor.o: gltf/accessor.h world/bufferview.h world/lists.h
world/buffer.o:  gltf/buffer.h
world/bufferview.o: gltf/bufferview.h world/buffer.h world/lists.h
world/lists.o: gltf.h world/accessor.h world/buffer.h world/bufferview.h world/mesh.h \
	world/node.h world/scene.h
world/mesh.o: gltf/mesh.h world/lists.h world/primitive.h
world/primitive.o: gltf/primitive.h world/accessor.h world/lists.h
world/node.o: gltf/node.h shader.h world/lists.h world/mesh.h
world/scene.o: gltf/scene.h shader.h world/node.h world/lists.h

.PHONY: clean

clean:
	rm -f $(OBJS) $(TARGETS)
