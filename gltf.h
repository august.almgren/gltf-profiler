#ifndef GLTF_H
#define GLTF_H

#include <stdio.h>
#include <stdlib.h>

#include "json.h"
#include "lib/json-parser/json.h"
#include "gltf/accessor.h"
#include "gltf/buffer.h"
#include "gltf/bufferview.h"
#include "gltf/mesh.h"
#include "gltf/node.h"
#include "gltf/scene.h"

struct gltf_t {

	unsigned int num_accessors;
	struct gltf_accessor_t **accessors;

	unsigned int num_buffers;
	struct gltf_buffer_t **buffers;

	unsigned int num_bufferviews;
	struct gltf_bufferview_t **bufferviews;

	unsigned int num_meshes;
	struct gltf_mesh_t **meshes;

	unsigned int num_nodes;
	struct gltf_node_t **nodes;

	unsigned int scene_id;

	unsigned int num_scenes;
	struct gltf_scene_t **scenes;
};

struct gltf_t *load_gltf(const char *fdir, const char *fname);
void free_gltf(struct gltf_t *gltf);

#endif // GLTF_H
