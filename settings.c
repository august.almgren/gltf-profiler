#include "settings.h"

static bool anti_aliasing_enabled;
static bool blending_enabled;
static bool backface_culling_enabled;
static bool depth_test_enabled;
static bool dither_enabled;
static bool polygon_offset_fill_enabled;
static bool sample_alpha_to_coverage_enabled;
static bool sample_coverage_enabled;
static bool scissor_test_enabled;
static bool stencil_test_enabled;
static bool max_fps_enabled;
static bool wireframe_mode_enabled;

void init_settings()
{
	anti_aliasing_enabled = false;
	glDisable(GL_POINT_SMOOTH);
	glDisable(GL_LINE_SMOOTH);
	glDisable(GL_POLYGON_SMOOTH);

	blending_enabled = false;
	glDisable(GL_BLEND);

	backface_culling_enabled = false;
	glDisable(GL_CULL_FACE);

	depth_test_enabled = false;
	glDisable(GL_DEPTH_TEST);

	dither_enabled = false;
	glDisable(GL_DITHER);

	polygon_offset_fill_enabled = false;
	glDisable(GL_POLYGON_OFFSET_FILL);

	sample_alpha_to_coverage_enabled = false;
	glDisable(GL_SAMPLE_ALPHA_TO_COVERAGE);

	sample_coverage_enabled = false;
	glDisable(GL_SAMPLE_COVERAGE);

	scissor_test_enabled = false;
	glDisable(GL_SCISSOR_TEST);

	stencil_test_enabled = false;
	glDisable(GL_STENCIL_TEST);

	max_fps_enabled = false;
	glfwSwapInterval(0);

	wireframe_mode_enabled = false;
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}

void toggle_anti_aliasing()
{
	anti_aliasing_enabled = !anti_aliasing_enabled;
	if (anti_aliasing_enabled) {
		printf("anti aliasing on\n");
		glEnable(GL_POINT_SMOOTH);
		glEnable(GL_LINE_SMOOTH);
		glEnable(GL_POLYGON_SMOOTH);
	} else {
		printf("anti aliasing off\n");
		glDisable(GL_POINT_SMOOTH);
		glDisable(GL_LINE_SMOOTH);
		glDisable(GL_POLYGON_SMOOTH);
	}
}

void toggle_blending()
{
	blending_enabled = !blending_enabled;
	if (blending_enabled) {
		printf("blending on\n");
		glEnable(GL_BLEND);
	} else {
		printf("blending off\n");
		glDisable(GL_BLEND);
	}
}
void toggle_backface_culling()
{
	backface_culling_enabled = !backface_culling_enabled;
	if (backface_culling_enabled) {
		printf("backface culling on\n");
		glEnable(GL_CULL_FACE);
	} else {
		printf("backface culling off\n");
		glDisable(GL_CULL_FACE);
	}
}
void toggle_depth_test()
{
	depth_test_enabled = !depth_test_enabled;
	if (depth_test_enabled) {
		printf("depth test on\n");
		glEnable(GL_DEPTH_TEST);
	} else {
		printf("depth test off\n");
		glDisable(GL_DEPTH_TEST);
	}
}
void toggle_dithering()
{
	dither_enabled = !dither_enabled;
	if (dither_enabled) {
		printf("dither on\n");
		glEnable(GL_DITHER);
	} else {
		printf("dither off\n");
		glDisable(GL_DITHER);
	}
}
void toggle_polygon_offset_fill()
{
	polygon_offset_fill_enabled = !polygon_offset_fill_enabled;
	if (polygon_offset_fill_enabled) {
		printf("polygon offset fill on\n");
		glEnable(GL_POLYGON_OFFSET_FILL);
	} else {
		printf("polygon offset fill off\n");
		glDisable(GL_POLYGON_OFFSET_FILL);
	}
}
void toggle_sample_alpha_to_coverage()
{
	sample_alpha_to_coverage_enabled = !sample_alpha_to_coverage_enabled;
	if (sample_alpha_to_coverage_enabled) {
		printf("sample alpha to coverage on\n");
		glEnable(GL_SAMPLE_ALPHA_TO_COVERAGE);
	} else {
		printf("sample alpha to coverage off\n");
		glDisable(GL_SAMPLE_ALPHA_TO_COVERAGE);
	}
}
void toggle_sample_coverage()
{
	sample_coverage_enabled = !sample_coverage_enabled;
	if (sample_coverage_enabled) {
		printf("sample coverage on\n");
		glEnable(GL_SAMPLE_COVERAGE);
	} else {
		printf("sample coverage off\n");
		glDisable(GL_SAMPLE_COVERAGE);
	}
}
void toggle_scissor_test()
{
	scissor_test_enabled = 	scissor_test_enabled;
	if (scissor_test_enabled) {
		printf("scissor test on\n");
		glEnable(GL_SCISSOR_TEST);
	} else {
		printf("scissor test off\n");
		glDisable(GL_SCISSOR_TEST);
	}
}
void toggle_stencil_test()
{
	stencil_test_enabled = !stencil_test_enabled;
	if (stencil_test_enabled) {
		printf("stencil test on\n");
		glEnable(GL_STENCIL_TEST);
	} else {
		printf("stencil test off\n");
		glDisable(GL_STENCIL_TEST);
	}
}

void toggle_max_fps()
{
	max_fps_enabled = !max_fps_enabled;
	if (max_fps_enabled) {
		printf("max fps on\n");
		glfwSwapInterval(1);
	} else {
		printf("max fps off\n");
		glfwSwapInterval(0);
	}
}

void toggle_wireframe_mode()
{
	wireframe_mode_enabled = !wireframe_mode_enabled;
	if (wireframe_mode_enabled) {
		printf("wireframe mode on\n");
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	} else {
		printf("wireframe mode off\n");
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}
}
