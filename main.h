#ifndef MAIN_H
#define MAIN_H

#include <GL/glew.h> // include before other GL headers
#include <GLFW/glfw3.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "gltf.h"
#include "settings.h"
#include "world/scene.h"
#include "shader.h"
#include "window.h"

int main();

#endif // MAIN_H
