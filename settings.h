#ifndef SETTINGS_H
#define SETTINGS_H

#include <GLFW/glfw3.h>
#include <stdbool.h>
#include <stdio.h>

void init_settings();

void toggle_anti_aliasing();
void toggle_blending();
void toggle_backface_culling();
void toggle_depth_test();
void toggle_dithering();
void toggle_polygon_offset_fill();
void toggle_sample_alpha_to_coverage();
void toggle_sample_coverage();
void toggle_scissor_test();
void toggle_stencil_test();
void toggle_max_fps();
void toggle_wireframe_mode();

#endif // SETTINGS_H
